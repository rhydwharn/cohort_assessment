const { defineConfig } = require("cypress");
const preprocessor = require("@badeball/cypress-cucumber-preprocessor");
const browserify = require("@badeball/cypress-cucumber-preprocessor/browserify");

async function setupNodeEvents(on, config) {
  await preprocessor.addCucumberPreprocessorPlugin(on, config);

  on('file:preprocessor', browserify.default(config));

  return config

}

module.exports = defineConfig({
  e2e: {
    baseUrl: "https://www.dummyticket.com/",
    viewportHeight: 800,
    viewportWidth: 1400,
    chromeWebSecurity: false,
    defaultCommandTimeout: 10000,
    experimentalModifyObstructiveThirdPartyCode: true,
    specPattern:'**/*.feature',
    setupNodeEvents,
  },
});
