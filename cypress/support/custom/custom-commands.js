let textData
let travelerDetails
let travelDetails
let billingInfo
let paymentMethod
let iframe_selector
let element_to_find_selector
let $emailBody

before(function () {
    cy.fixture('buy_ticket').then(
        selector => {
            textData = selector,
            travelerDetails = selector.travelerDetails
            travelDetails = selector.travelDetails
            billingInfo = selector.billingDetails
            paymentMethod = selector.paymentMethod
        }
    )
})

Cypress.Commands.add('clickAnyElement', (element) => {
    cy.get(element).should('exist').click({ multiple: true });
})

Cypress.Commands.add('typeText', (element, text) => {
    cy.get(element).should('exist').type(text)
})

Cypress.Commands.add("insertText", (string) => {
    switch (string) {
        case 'first name':
            cy.typeText(travelerDetails.firstName, 'Ridwan')
        break
        case 'last name':
            cy.typeText(travelerDetails.lastName, 'Abdulazeez')
            break
        case "origination city":
            cy.typeText(travelDetails.origin, 'LOS')
            break
        case "destination city":
            cy.typeText(travelDetails.destination, 'LON')
            break
        case "billing name":
            cy.typeText(billingInfo.billingName, 'Ridwan Abdulazeez')
            break
        case "billing phone number":
            cy.typeText(billingInfo.billingPhone, '+2348091691075')
            break
        case "billing email address":
            cy.typeText(billingInfo.billingEmail, 'ridwan.abdulazeez1@gmail.com')
            break
        case "billing address":
            cy.typeText(billingInfo.billingAddress, '51, Alaja nibon avenue, ilu awon osu estate')
            break
        case "billing town":
            cy.typeText(billingInfo.billingCity, 'Opa Ifa')
        // case "billing postcode": 
        //     cy.typeText(billingInfo.billingPostCode, '100261')
        break;
        case "card number":
      cy.get('div#yith-stripe-card-number  iframe[role="presentation"]').checkiFrameStatus().
      find('[name="cardnumber"]').type('4084084084084081')
      break;
        case "card expiry date":
      cy.get('div#yith-stripe-card-number  iframe[role="presentation"]').checkiFrameStatus().
      find('[autocomplete="cc-csc"]').type('0129', {force: true})
      break;
        case "card CVV":
      cy.get('div#yith-stripe-card-expiry  iframe[role="presentation"]').checkiFrameStatus().
      find('[autocomplete="cc-exp"]').type('0425', {force: true})

    }
})

Cypress.Commands.add("selectItem", (item) => {
    switch (item) {
        case 'date of birth':
            cy.get(travelerDetails.dob).type('08/05/2019', {force: true});
            cy.clickAnyElement(travelerDetails.doneButton)
            break
        case 'sex':
            cy.get(travelerDetails.sex).check('1')
            break
        case 'departure date':
            cy.get(travelDetails.departureDate).type('01/10/2024', {force: true})
            cy.clickAnyElement(travelDetails.doneButton)
            break
        case 'billing country':
            cy.get(billingInfo.billingCountry).select('Nigeria', {force: true})
            break
        case 'billing state':
            cy.get(billingInfo.billingState).select('Adamawa', {force: true})
            break
        
        case 'cards as payment method':
            cy.get(paymentMethod.paymentType).check('yith-stripe')
            break
    }
})