import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

let homePage
let travelerSection
let visaSection
let paymentSection

before(() => {
  cy.fixture("home_elements").then((homeSelector) => {
    homePage = homeSelector;
  })

  cy.fixture("buy_ticket").then((buyTicketSelector) => {
    visaSection = buyTicketSelector.visa_type;
    travelerSection = buyTicketSelector.travelerDetails;
    paymentSection = buyTicketSelector.paymentMethod;
  })
})


Given(/^I am a guest user, I have internet access$/, () => {
	return true;
});

Then (/^I am able to launch the dummy ticket website$/, () => {
  cy.visit("/");
   cy.clickAnyElement(homePage.buy_ticket_button)
 
});



Then(/^I am already on the dummy ticket webpage$/, () => {
	return true;
});

Then (/^I click on visa type I want to purhase$/, (args1) => {
	cy.clickAnyElement(visaSection.hotel_booking)
});


Then(/^I input "([^"]*)"$/, (args1) => {
	switch(args1){
    case "first name":
    cy.insertText(args1)
    break
    case "last name":
      cy.insertText(args1)
      break
      case "origination city":
        cy.insertText(args1)
        break
        case "destination city":
      cy.insertText(args1)
      break
      case "billing name":
      cy.insertText(args1)
      break
      case "billing phone number":
      cy.insertText(args1)
      break
      case "billing email address":
      cy.insertText(args1)
      break
      case "billing address":
      cy.insertText(args1)
      break
      case "billing town":
      cy.insertText(args1)
      break
      case "card number":
      cy.insertText(args1)
      break
      case "card expiry date":
      cy.insertText(args1)
      break
      case "card CVV":
        cy.insertText(args1)
        break
  }
      
  
});

Then(/^I select "([^"]*)"$/, (args1) => {
	switch(args1) {
    case "date of birth": 
    cy.selectItem(args1)
    break
    case "sex": 
    cy.selectItem(args1)
    break
    case "departure date": 
    cy.selectItem(args1)
    break
    case "billing country": 
    cy.selectItem(args1)
    break
    case "billing state": 
    cy.selectItem(args1)
    break
    case "cards as payment method": 
    cy.selectItem(args1)
    break
  }
});



Then(/^I click "([^"]*)"$/, (args1) => {
	cy.clickAnyElement(paymentSection.submitButton).wait(5000)
});



Then(/^I validate response$/, () => {
	cy.get('.woocommerce-error').should('be.visible')
});
