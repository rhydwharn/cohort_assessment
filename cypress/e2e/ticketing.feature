Feature: Access Dummy Ticket Website

    As a guest user, I want to access the Dummy ticket Website

    Scenario: Access dummy ticket website

    Given I am a guest user, I have internet access
    And I am able to launch the dummy ticket website
    And I am already on the dummy ticket webpage
    And I click on visa type I want to purhase
    And I input "first name"
    And I input "last name"
    And I select "date of birth"
    And I select "sex"
    And I input "origination city"
    And I input "destination city"
    And I select "departure date"
    And I input "billing name"
    And I input "billing phone number"
    And I input "billing email address"
    And I select "billing country"
    And I input "billing address"
    And I input "billing town"
    And I select "billing state"
    And I select "cards as payment method"
    And I input "card number"
    And I input "card expiry date"
    And I input "card CVV"
    And I click "submit button"
    And I validate response